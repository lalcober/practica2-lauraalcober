﻿Practica 2: Herramientas de desarrollo
Entornos de dearrollo

Proyecto 🚀

El proyecto está compuesto por:
	2 ventanas de escritorio simulando funciones de la aplicación (Nesto) creada en el Proyecto 1 de la asignatura:
		- 1 aplicación de ventana creada en Eclipse con 2 ejecutables (VentanaEclipse.exe y VentanaEclipse.jar)
		- 1 aplicación de ventana creada en Visual Studio con 1 ejecutable (WindowsFormsApp.exe)
	1 aplicación de consola ejecutable (.exe)
	1 archivo de librería de java (libreriaEclipse.jar)
	1 archivo de libreria de C# (Libreria.dll)

El repositorio de git es público y por tanto se puede descargar y acceder a él sin problemas.


Pre-requisitos 📋

Un ordenador con un sistema operativo que pueda ejecutar archivos .exe
JRE 1.8 o superior


Construido con 🛠️

Eclipse - Ventana de Java, Aplicación de consola de Java, ejecutable .jar y librería.jar
Visual Studio - Ventana de C# y librería .dll
Launch4J - Ejecutables .exe de Java a partir de .jar


Autores ✒️

Laura Alcober - Bitbucket: lalcober | Mail: laalpri@gmail.com
