package consola;

import java.util.Scanner;

import eclipseLibreria.EclipseLibreria;

public class Consola {

	public static void main(String[] args) {
		
		System.out.println("1-Escribir fecha\n2-Multiplica un array\n3-Matriz de numeros aleatorios\n4-Pasar array a cadena\n5-Salir");

		Scanner lector = new Scanner(System.in);
		int opcion = 0;
		
		while (opcion!=5) {
			
			System.out.println("Introduce una opcion");
			opcion = lector.nextInt();
			lector.nextLine();

			switch (opcion) {

			case 1:
				System.out.println("Introduce dia");
				int dia = lector.nextInt();
				System.out.println("Introduce mes");
				int mes = lector.nextInt();
				System.out.println("Introduce anno");
				int anno = lector.nextInt();

				System.out.println(EclipseLibreria.escribirFecha(anno, mes, dia));

				break;
			case 2:

				int[] array = new int[10];

				for (int i = 0; i < array.length; i++) {
					System.out.println("Introduce un numero");
					array[i] = lector.nextInt();
				}
				System.out.println("Introduce un numero para multiplicar");
				int multiplo = lector.nextInt();

				EclipseLibreria.multiplicarArray(array, multiplo);

				for (int i = 0; i < array.length; i++) {
					System.out.print(array + " ");
				}

				break;
			case 3:

				System.out.println("Introduce el numero de filas");
				int filas = lector.nextInt();
				System.out.println("Introduce el numero de columnas");
				int columnas = lector.nextInt();

				int[][] matrizRandom = EclipseLibreria.matrizAleatoria(filas, columnas);
				for (int i = 0; i < matrizRandom.length; i++) {
					for (int j = 0; j < matrizRandom[i].length; j++) {
						System.out.print(matrizRandom[i][j] + " ");
					}
					System.out.println();
				}

				break;
			case 4:
				String[] cadenas = new String[10];

				for (int i = 0; i < cadenas.length; i++) {
					System.out.println("Introduce una cadena");
					cadenas[i] = lector.nextLine();
				}
				System.out.println(EclipseLibreria.pasarAString(cadenas));

				break;
				
			case 5:
				break;
			default:
				System.out.println("Opcion no permitida");
				break;

			}
		}
		

		lector.close();

	}

}
