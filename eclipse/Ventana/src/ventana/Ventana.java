package ventana;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import javax.swing.JToggleButton;


public class Ventana extends JFrame {

        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private JPanel contentPane;
        private JPasswordField passwordField;
        private JTextField textField;


        /**
         * Launch the application.
         */
        public static void main(String[] args) {
                EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                try {
                                        Ventana frame = new Ventana();
                                        frame.setVisible(true);
                                } catch (Exception e) {
                                        e.printStackTrace();
                                }
                        }
                });
        }

        /**
         * Create the frame.
         */
        public Ventana() {
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setBounds(100, 100, 632, 413);
                contentPane = new JPanel();
                contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
                setContentPane(contentPane);
                contentPane.setLayout(null);

                JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
                tabbedPane.setBounds(6, 6, 620, 379);
                contentPane.add(tabbedPane);

                JPanel panel = new JPanel();
                panel.setBackground(Color.GRAY);
                tabbedPane.addTab("Usuario", null, panel, null);
                panel.setLayout(null);


                JLabel lblNewLabel = new JLabel("");
                lblNewLabel.setIcon(new ImageIcon(Ventana.class.getResource("/recursos/iconfinder_user_285655.png")));
                lblNewLabel.setBounds(25, 48, 128, 128);
                panel.add(lblNewLabel);

                JLabel lblNewLabel_1 = new JLabel("Usuario");
                lblNewLabel_1.setForeground(new Color(255, 215, 0));
                lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
                lblNewLabel_1.setBounds(61, 172, 61, 16);
                panel.add(lblNewLabel_1);

                JButton btnCerrarSesion = new JButton("Cerrar sesion");
                btnCerrarSesion.setBounds(29, 193, 117, 29);
                panel.add(btnCerrarSesion);

                JLabel lblPais = new JLabel("Pais");
                lblPais.setBounds(245, 18, 61, 16);
                panel.add(lblPais);

                JComboBox<String> comboBox = new JComboBox<String>();

                comboBox.setBounds(203, 33, 198, 27);

                comboBox.setBounds(240, 36, 182, 26);
                panel.add(comboBox);

                comboBox.addItem("Spain");
                comboBox.addItem("France");
                comboBox.addItem("Germany");
                comboBox.addItem("UK");
                comboBox.addItem("Italy");

                JLabel lblCambiarPassword = new JLabel("Cambiar password");
                lblCambiarPassword.setBounds(245, 68, 155, 16);
                panel.add(lblCambiarPassword);

                passwordField = new JPasswordField();
                passwordField.setBounds(245, 88, 171, 26);
                panel.add(passwordField);

                JLabel lblNickname = new JLabel("Nickname");
                lblNickname.setBounds(246, 120, 155, 16);
                panel.add(lblNickname);

                textField = new JTextField();
                textField.setBounds(247, 140, 166, 26);
                panel.add(textField);
                textField.setColumns(10);

                JLabel lblPerfil = new JLabel("Perfil");
                lblPerfil.setBounds(246, 172, 61, 16);
                panel.add(lblPerfil);

                JRadioButton rdbtnPublico = new JRadioButton("Publico");
                rdbtnPublico.setBounds(239, 192, 78, 23);
                panel.add(rdbtnPublico);

                JRadioButton rdbtnPrivado = new JRadioButton("Privado");
                rdbtnPrivado.setBounds(322, 193, 78, 23);
                panel.add(rdbtnPrivado);

                JLabel lblDescripcion = new JLabel("Descripcion");
                lblDescripcion.setBounds(245, 227, 117, 16);
                panel.add(lblDescripcion);

                JTextArea textArea = new JTextArea();
                textArea.setBounds(248, 248, 212, 79);
                panel.add(textArea);

                JPanel panel_1 = new JPanel();
                panel_1.setBackground(Color.GRAY);
                tabbedPane.addTab("Audio", null, panel_1, null);
                panel_1.setLayout(null);

                JCheckBox chckbxFrontales = new JCheckBox("Frontales");
                chckbxFrontales.setBackground(Color.ORANGE);
                chckbxFrontales.setBounds(64, 66, 145, 23);
                panel_1.add(chckbxFrontales);

                JSpinner spinner = new JSpinner();
                spinner.setBounds(82, 207, 101, 23);
                panel_1.add(spinner);


                JSlider slider1 = new JSlider();
                slider1.setMajorTickSpacing(10);
                slider1.setMinorTickSpacing(5);
                slider1.setSnapToTicks(true);
                slider1.setPaintTicks(true);
                slider1.setOrientation(SwingConstants.VERTICAL);
                slider1.setBounds(527, 48, 29, 248);
                panel_1.add(slider1);

                JSlider slider_11 = new JSlider();
                slider_11.setSnapToTicks(true);
                slider_11.setPaintTicks(true);
                slider_11.setOrientation(SwingConstants.VERTICAL);
                slider_11.setMinorTickSpacing(5);
                slider_11.setMajorTickSpacing(10);
                slider_11.setBounds(486, 48, 29, 248);
                panel_1.add(slider_11);

                JSlider slider_21 = new JSlider();
                slider_21.setSnapToTicks(true);
                slider_21.setPaintTicks(true);
                slider_21.setOrientation(SwingConstants.VERTICAL);
                slider_21.setMinorTickSpacing(5);
                slider_21.setMajorTickSpacing(10);
                slider_21.setBounds(445, 48, 29, 248);
                panel_1.add(slider_21);

                JSlider slider_31 = new JSlider();
                slider_31.setPaintTicks(true);
                slider_31.setSnapToTicks(true);
                slider_31.setOrientation(SwingConstants.VERTICAL);
                slider_31.setMinorTickSpacing(5);
                slider_31.setMajorTickSpacing(10);
                slider_31.setBounds(404, 48, 29, 248);
                panel_1.add(slider_31);

                JLabel lblEcualizador1 = new JLabel("Ecualizador");
                lblEcualizador1.setHorizontalAlignment(SwingConstants.CENTER);
                lblEcualizador1.setBounds(428, 23, 103, 16);
                panel_1.add(lblEcualizador1);

                JLabel lblAltavoces1 = new JLabel("Altavoces");
                lblAltavoces1.setBounds(67, 35, 61, 16);
                panel_1.add(lblAltavoces1);

                JCheckBox chckbxTraseros1 = new JCheckBox("Traseros");
                chckbxTraseros1.setBackground(Color.ORANGE);
                chckbxTraseros1.setBounds(64, 89, 145, 23);
                panel_1.add(chckbxTraseros1);

                JCheckBox chckbxLateralIzquierdo = new JCheckBox("Lateral izquierdo");
                chckbxLateralIzquierdo.setBackground(Color.ORANGE);
                chckbxLateralIzquierdo.setBounds(64, 112, 145, 23);
                panel_1.add(chckbxLateralIzquierdo);

                JCheckBox chckbxLateralDerecho = new JCheckBox("Lateral derecho");
                chckbxLateralDerecho.setBackground(Color.ORANGE);
                chckbxLateralDerecho.setBounds(64, 135, 145, 23);
                panel_1.add(chckbxLateralDerecho);

                JLabel lblVolumen1 = new JLabel("Volumen");
                lblVolumen1.setBounds(99, 188, 61, 16);
                panel_1.add(lblVolumen1);

                JLabel lblSilenciar1 = new JLabel("Silenciar");
                lblSilenciar1.setHorizontalAlignment(SwingConstants.CENTER);
                lblSilenciar1.setBounds(98, 251, 61, 16);
                panel_1.add(lblSilenciar1);

                JToggleButton toggleButton = new JToggleButton("");
                toggleButton.setIcon(new ImageIcon(Ventana.class.getResource("/recursos/iconfinder_mute_172512-2.png")));
                toggleButton.setBounds(94, 278, 68, 28);
                panel_1.add(toggleButton);

        }
}
