﻿using System;

namespace Libreria
{
    public class Class1
    {

        public static String EscribirFecha(int anno, int mes, int dia)
        {
            String fecha = dia + "-" + mes + "-" + anno;
            return fecha;
        }

        public static void MultiplicarArray(int[] array, int numero)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = array[i] * numero;
            }
        }

        public static int[,] MatrizAleatoria(int filas, int columnas)
        {
            Random random = new Random();
            int[,] matriz = new int[filas , columnas];
            for(int i = 0; i < filas; i++)
            {
                for(int j = 0; j<columnas; j++)
                {
                    
                    matriz[i, j] = random.Next(1, 100);
                }
            }
            return matriz;

        }

        public static String PasarAString(String [] array)
        {
            String cadena = "";
            for (int i = 0; i<array.Length; i++)
            {
                if (i == 0)
                {
                    cadena = array[i];
                }
                else
                {
                    cadena = cadena + " " + array[i];
                }
            }
            return cadena;
        }
    }
}
